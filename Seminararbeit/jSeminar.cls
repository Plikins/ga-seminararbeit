%------------------------------------------------
% LaTeX-Klasse für Seminararbeiten.
% Autor: Jakob Bossek
%
% HINWEIS: BITTE ÄNDERT DIESE DATEI NICHT! Sollten
% weitere Pakete benötigt werden, so fügt diese
% bitte nach der Zeile \documentclass{jSeminar}
% in der template.tex Datei ein!
%------------------------------------------------
\ProvidesClass{jSeminar}[2016/22/10 version 1.00 Script]

\LoadClass[a4paper,11pt,ngerman]{article}
\usepackage{scrhack}
\usepackage{floatrow}
\usepackage[a4paper,left=3.5cm,right=3.5cm,bottom=3.5cm,top=3cm]{geometry}
\usepackage{graphics}

\usepackage[utf8]{inputenc}
\usepackage[ngerman]{babel}
\usepackage{lmodern}

\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{enumerate}

\usepackage{todonotes} % Dummy-Bilder, \listoftodos
\usepackage{adjustbox}

\newcommand{\todofig}[1]{\adjustbox{margin=1em,width=#1,set height=1cm,set depth=1cm, frame}{Placeholder}}

\usepackage{titlesec}
\titleformat{\chapter}[display]
  {\normalfont\Large\raggedleft}
  {\MakeUppercase{\chaptertitlename}%
    \rlap{ \resizebox{!}{1.5cm}{\thechapter} \rule{5cm}{1.5cm}}}
  {10pt}{\Huge}
\titlespacing*{\chapter}{0pt}{30pt}{20pt}

\usepackage[autostyle=true,german=quotes]{csquotes}

\usepackage{algpseudocode,algorithm,algorithmicx}
\newcommand*\Let[2]{\State #1 $\gets$ #2}
\algrenewcommand\algorithmicrequire{\textbf{Vorbedingung:}}
\algrenewcommand\algorithmicensure{\textbf{Nachbedingung:}}

\newcommand{\bigO}{\mathcal{O}}

\usepackage{amsmath, amssymb, amsthm}

\newtheorem{definition}{Definition}
\newtheorem{folgerung}{Folgerung}
\newtheorem{korollar}{Korollar}
\newtheorem{lemma}{Lemma}
\newtheorem{satz}{Satz}

\usepackage{setspace}
\onehalfspacing
\parindent 0pt

\usepackage{xcolor}
\definecolor{mDarkBrown}{HTML}{604c38}
\definecolor{mDarkTeal}{HTML}{23373b}
\definecolor{mLightBrown}{HTML}{EB811B}
\definecolor{mLightGreen}{HTML}{14B03D}
\definecolor{mBackground}{HTML}{FFFFFF}

\usepackage{listings}
\lstset{%
  language=[LaTeX]{TeX},
  basicstyle=\ttfamily,
  keywordstyle=\color{mLightBrown}\bfseries,
  commentstyle=\color{mLightGreen},
  stringstyle=\color{mLightGreen},
  backgroundcolor=\color{mBackground},
  numbers=none,
  numberstyle=\tiny\ttfamily,
  stepnumber=2,
  showspaces=false,
  showstringspaces=false,
  showtabs=false,
  frame=none,
  framerule=1pt,
  tabsize=2,
  rulesep=5em,
  captionpos=b,
  breaklines=true,
  breakatwhitespace=false,
  framexleftmargin=0em,
  framexrightmargin=0em,
  xleftmargin=0em,
  xrightmargin=0em,
  aboveskip=1em,
  belowskip=1em,
  morekeywords={usetheme,institute,maketitle,mthemetitleformat,plain,setbeamercolor},
}
%\lstMakeShortInline|

\usepackage[colorlinks=true,
            linkcolor=gray!85,
            menucolor=gray!85,
            urlcolor=gray!85,
            citecolor=gray!85]{hyperref}

\usepackage{tikz}
\usetikzlibrary{backgrounds, arrows, petri, topaths, calc, patterns, decorations.pathreplacing}
